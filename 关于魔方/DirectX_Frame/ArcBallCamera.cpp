#include "ArcBallCamera.h"

ABCamera::ABCamera(float windowWidth,float windowHeight,float distance,D3DXVECTOR3 target):
	_RotateCamera(false),
	_distance(distance),_target(target),
	_windowWidth(windowWidth),_windowHeight(windowHeight)
{
	_pArcBallForCamera=new ArcBall(windowWidth,windowHeight);
	D3DXQuaternionIdentity(&_CurrentQuaternion);
}

ABCamera::~ABCamera()
{
	SAFE_DELETE(_pArcBallForCamera);
}

void ABCamera::HandleMessages(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam,LPDIRECT3DDEVICE9 pd3dDevice)
{
	int mouseX=(short)LOWORD(lParam);
	int mouseY=(short)HIWORD(lParam);
	switch(message)
	{
	case WM_RBUTTONDOWN:
		if(mouseX>0&&mouseX<_windowWidth&&mouseY>0&&mouseY<_windowHeight)
		{
			_RotateCamera=true;
			_BeginPoint=_pArcBallForCamera->ScreenToBall(mouseX,mouseY);
			_LastQuaternion=_CurrentQuaternion;
			SetCapture(hwnd);
		}

		break;

	case WM_MOUSEMOVE:
		if(_RotateCamera)
		{
			_CurrentPoint=_pArcBallForCamera->ScreenToBall(mouseX,mouseY);
			_CurrentQuaternion=_LastQuaternion*_pArcBallForCamera->QuatFromBallPoints(_BeginPoint,_CurrentPoint);
			D3DXMATRIX view;
			CalculateCamera(view);
			pd3dDevice->SetTransform(D3DTS_VIEW,&view);
		}
		break;

	case WM_RBUTTONUP:
		_RotateCamera=false;
		ReleaseCapture();
		break;
	case WM_MOUSEWHEEL:
		_distance-=short(HIWORD(wParam))*_distance/3600.0f;
		_distance=max(_distance,MAX_DISTANCE);
		_distance=min(_distance,MIN_DISTANCE);
		D3DXMATRIX view;
		CalculateCamera(view);
		pd3dDevice->SetTransform(D3DTS_VIEW,&view);
	}

}

void ABCamera::CalculateCamera(D3DXMATRIX &out)
{
	D3DXMATRIX rotate;
	D3DXMatrixRotationQuaternion(&rotate,&_CurrentQuaternion);
	D3DXMatrixInverse(&rotate,NULL,&rotate);

	D3DXVECTOR3 up(0,1,0);
	D3DXVec3TransformNormal(&up,&up,&rotate);
	D3DXVECTOR3 look(0,0,1);
	D3DXVec3TransformNormal(&look,&look,&rotate);
	_position=_target-look*_distance;

	D3DXMatrixLookAtLH(&out,&_position,&_target,&up);
}