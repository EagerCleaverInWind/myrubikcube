#include "Math.h"


Ray CaculatePickingRay(LPDIRECT3DDEVICE9 d3ddevice_,int x,int y)
{
	float px = 0.0f;
	float py = 0.0f;

	// Get viewport
	D3DVIEWPORT9 vp;
	d3ddevice_->GetViewport(&vp);

	// Get Projection matrix
	D3DXMATRIX proj;
	d3ddevice_->GetTransform(D3DTS_PROJECTION, &proj);
	float t=proj(2,3);
	px = ((( 2.0f*x) / vp.Width)  - 1.0f) / proj(0, 0);
	py = (((-2.0f*y) / vp.Height) + 1.0f) / proj(1, 1);

	Ray ray;
	ray.origin    = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	ray.direction = D3DXVECTOR3(px, py, 1.0f); 

	// Get view matrix
	D3DXMATRIX view;
	d3ddevice_->GetTransform(D3DTS_VIEW, &view);

	// Get world matrix
	D3DXMATRIX world;
	D3DXMatrixIdentity(&world);//一开始用的是d3ddevice_->GetTransform(D3DTS_WORLD, &world)，然后各种离谱的拾取面……
	//出现“离奇”时设断点各种跟踪,终于有一次发现特么拾取射线的原点变了（变动很大的那种）而我根本没动摄像机，才找出原因
	//因为拾取射线最终是变换到RubikCube的模型坐标系下检测，而整个RubikCube是不做世界变换的，
	//最后一次（上一次）设置世界矩阵是绘制最后一个小立方体时，这个世界矩阵并不是对应整个魔方的

	// Concatinate them in to single matrix
	D3DXMATRIX WorldView = world * view;

	// inverse it
	D3DXMATRIX worldviewInverse;
	D3DXMatrixInverse(&worldviewInverse, 0, &WorldView);


	// Transform the ray to model space
	D3DXVec3TransformCoord(&ray.origin, &ray.origin, &worldviewInverse) ;
	D3DXVec3TransformNormal(&ray.direction, &ray.direction, &worldviewInverse) ;

	// Normalize the direction
	D3DXVec3Normalize(&ray.direction, &ray.direction) ;

	return ray;
}

D3DXVECTOR3 ScreenToWorld(LPDIRECT3DDEVICE9 d3ddevice_,int x,int y)
{
	D3DXVECTOR3 vector3 ;

	// Get viewport
	D3DVIEWPORT9 vp;
	d3ddevice_->GetViewport(&vp);

	// Get Projection matrix
	D3DXMATRIX proj;
	d3ddevice_->GetTransform(D3DTS_PROJECTION, &proj);

	vector3.x = ((( 2.0f*x) / vp.Width)  - 1.0f) / proj(0, 0);
	vector3.y = (((-2.0f*y) / vp.Height) + 1.0f) / proj(1, 1);
	vector3.z = 1.0f ;

	// Get view matrix
	D3DXMATRIX view;
	d3ddevice_->GetTransform(D3DTS_VIEW, &view);
	// inverse it
	D3DXMATRIX viewInverse;
	D3DXMatrixInverse(&viewInverse, 0, &view);

	D3DXVec3TransformCoord(&vector3, &vector3, &viewInverse) ;

	D3DXVec3Normalize(&vector3, &vector3) ;

	return vector3 ;
}

bool RayTriangleIntersection(Ray* ray, Triangle* triangle, D3DXVECTOR3* hit_point)
{
	D3DXVECTOR3 orig = ray->origin;
	D3DXVECTOR3 dir  = ray->direction;

	D3DXVECTOR3 v0 = triangle->v1;
	D3DXVECTOR3 v1 = triangle->v2;
	D3DXVECTOR3 v2 = triangle->v3;

	// E1
	D3DXVECTOR3 E1 = v1 - v0;

	// E2
	D3DXVECTOR3 E2 = v2 - v0;

	// P
	D3DXVECTOR3 P;
	D3DXVec3Cross(&P, &dir, &E2);

	// determinant
	//float det = E1.Dot(P);
	float det = D3DXVec3Dot(&E1, &P);

	// keep det > 0, modify T accordingly
	D3DXVECTOR3 T;
	if( det >0 )
	{
		T = orig - v0;
	}
	else
	{
		T = v0 - orig;
		det = -det;
	}

	// If determinant is near zero, ray lies in plane of triangle
	if( det < 0.0001f )
		return false;

	// Calculate u and make sure u <= 1
	float u = D3DXVec3Dot(&T, &P);
	if( u < 0.0f || u > det )
		return false;

	// Q
	D3DXVECTOR3 Q;
	D3DXVec3Cross(&Q, &T, &E1);

	// Calculate v and make sure u + v <= 1
	float v = D3DXVec3Dot(&dir, &Q);
	if( v < 0.0f || u + v > det )
		return false;

	// Calculate t, scale parameters, ray intersects triangle
	float t = D3DXVec3Dot(&E2, &Q);

	float fInvDet = 1.0f / det;
	t *= fInvDet;
	u *= fInvDet;
	v *= fInvDet;

	// Intersection point can be calculate by the following two ways
	// 1. O + Dt
	// 2. (1 - u - v) * v0 + u * v1 + v * v2
	// I use the first way.
	*hit_point = orig + (t * dir);

	return true;
}

bool RayRectIntersection(Ray& ray, Rect& rect, D3DXVECTOR3& hit_point)
{
	// Divide the rectangle into two triangles
	Triangle t1(rect.v1, rect.v2, rect.v3);
	Triangle t2(rect.v1, rect.v3, rect.v4) ;

	return RayTriangleIntersection(&ray, &t1, &hit_point) || RayTriangleIntersection(&ray, &t2, &hit_point);
}