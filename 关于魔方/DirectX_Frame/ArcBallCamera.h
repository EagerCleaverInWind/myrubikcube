#pragma once

#include "Utility.h"
#include "ArcBall.h"

class ABCamera
{
private:
	ArcBall  *_pArcBallForCamera;

	float _distance;
	D3DXVECTOR3 _target;
	D3DXVECTOR3 _position;

	bool _RotateCamera;
	D3DXVECTOR3		_BeginPoint;
	D3DXVECTOR3		_CurrentPoint;
	D3DXQUATERNION		_LastQuaternion;
	D3DXQUATERNION		_CurrentQuaternion;

	float _windowWidth,_windowHeight;


private:
	void CalculateCamera(D3DXMATRIX &_out);


public:
	ABCamera(float windowWidth,float windowHeight,float distance,D3DXVECTOR3 target=D3DXVECTOR3(0,0,0));
	~ABCamera();

	void  HandleMessages(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam,LPDIRECT3DDEVICE9 pd3dDevice);
};