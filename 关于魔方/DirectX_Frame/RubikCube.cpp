#include "RubikCube.h"

RubikCube::RubikCube(LPDIRECT3DDEVICE9 device,float windowWidth,float windowHeight):
	m_pDevice(device),
	m_WindowWidth(windowWidth),m_WindowHeight(windowHeight),
	m_isHit(false),m_rotateFinish(true),m_cubesSelected(false),
	m_RotateSpeed(1.0f)
{
	m_pCubes=new Cube[27];
	m_CubeGap=CUBE_GAP;
	m_TexWidth=TEX_WIDTH;
	m_TexHeight=TEX_HEIGHT;
	m_SideWidth=SIDE_WIDTH;
	m_Group=m_CubeGap+m_pCubes[0].GetLength();
	m_HalfLength=(m_pCubes[0].GetLength()*3+m_CubeGap*2)/2.0f;
	InitCubes();

	m_pArcBall=new ArcBall(windowWidth,windowHeight);

	// Calculate the coordinates of the 8 corner points on Rubik Cube, we use them to mark the Face coordinates later.
	D3DXVECTOR3 A(-m_HalfLength,  m_HalfLength, -m_HalfLength); // The front-top-left corner
	D3DXVECTOR3 B( m_HalfLength,  m_HalfLength, -m_HalfLength);
	D3DXVECTOR3 C( m_HalfLength, -m_HalfLength, -m_HalfLength);
	D3DXVECTOR3 D(-m_HalfLength, -m_HalfLength, -m_HalfLength);

	D3DXVECTOR3 E(-m_HalfLength,  m_HalfLength,  m_HalfLength); // The back-top-left corner
	D3DXVECTOR3 F( m_HalfLength,  m_HalfLength,  m_HalfLength);
	D3DXVECTOR3 G( m_HalfLength, -m_HalfLength,  m_HalfLength);
	D3DXVECTOR3 H(-m_HalfLength, -m_HalfLength,  m_HalfLength);

	// Initialize the 6 m_pFaces of Rubik Cube, m_pFaces used later in Ray-Cube hit test.
	Rect  FrontFace(A, B, C, D) ; 
	Rect   BackFace(E, F, G, H) ;
	Rect   LeftFace(E, A, D, H) ;
	Rect  RightFace(B, F, G, C) ;
	Rect    TopFace(E, F, B, A) ;
	Rect BottomFace(G, H, D, C) ;

	m_pFaces[0] = FrontFace;
	m_pFaces[1] = BackFace;
	m_pFaces[2] = LeftFace;
	m_pFaces[3] = RightFace;
	m_pFaces[4] = TopFace;
	m_pFaces[5] = BottomFace;
}

RubikCube::~RubikCube()
{
	m_pCubes[0].ReleaseStaticResources();
	if(m_pCubes!=NULL)
	{
		delete[]  m_pCubes;
		m_pCubes=NULL;
	}//麻痹，一开始用的是SAFE_DELETE,并不适用于new 数组

	SAFE_DELETE(m_pArcBall);

}

void RubikCube::InitCubes()
{
	//device & position	& buffer & textureId
	float cubeLength=m_pCubes[0].GetLength();

	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
			for (int k = 0; k < 3; k++)
			{
				int index=i*9+j*3+k;
				float x=(cubeLength+m_CubeGap)*k-m_HalfLength;
				float y=(cubeLength+m_CubeGap)*j-m_HalfLength;
				float z=(cubeLength+m_CubeGap)*i-m_HalfLength;
				m_pCubes[index].Init(m_pDevice,D3DXVECTOR3(x,y,z));

				int faceId;
				if(i!=1)//front face & back face
				{
						faceId=i>1?1:0;
						m_pCubes[index].SetTextureId(faceId,faceId);
				}
				if(k!=1)//left face & right face
				{
					faceId=k>1?3:2;
					m_pCubes[index].SetTextureId(faceId,faceId);
				}
				if(j!=1)//top face & bottom face
				{
					faceId=j>1?4:5;
					m_pCubes[index].SetTextureId(faceId,faceId);
				}
			}

	//layerId
	UpdateLayerId();

	//static textures
	LPDIRECT3DTEXTURE9	faceTextures[6];
	
	D3DCOLOR	faceColors[6]=
	{
		0xffffffff, // White,   front face
		0xffffff00, // Yellow,	back face
		0xffff0000, // Red,		left face
		0xffffa500,	// Orange,	right face
		0xff00ff00, // Green,	top face
		0xff0000ff, // Blue,	bottom face
	};
	for(int i=0;i<6;i++)
		faceTextures[i]=m_pCubes[0].CreateTexture(m_TexHeight,m_TexHeight,faceColors[i]);
	m_pCubes[0].SetFaceTextures(faceTextures);

	LPDIRECT3DTEXTURE9 innerTex=m_pCubes[0].CreateTexture(m_TexHeight,m_TexHeight,D3DCOLOR_XRGB(192,192,192),0);
	m_pCubes[0].SetInnerTexture(innerTex);


}

void RubikCube::UpdateLayerId()
{
	float length = m_pCubes[0].GetLength();

	for (int i = 0; i < 27; ++i)
	{
		float center_x = m_pCubes[i].GetCenter().x + m_HalfLength;
		float center_y = m_pCubes[i].GetCenter().y + m_HalfLength;
		float center_z = m_pCubes[i].GetCenter().z + m_HalfLength;

		for (int j = 0; j < 3; ++j)
		{
			if (center_x >= j * (length + m_CubeGap) 
				&& center_x <= (j + 1) * (length + m_CubeGap) - m_CubeGap)
			{
				m_pCubes[i].SetLayerX(j);
			}

			if (center_y >= j * (length + m_CubeGap)
				&& center_y <= (j + 1) * (length + m_CubeGap) - m_CubeGap)
			{
				m_pCubes[i].SetLayerY(j + 3);
			}

			if (center_z >= j * (length + m_CubeGap)
				&& center_z <= (j + 1) * (length + m_CubeGap) - m_CubeGap)
			{
				m_pCubes[i].SetLayerZ(j + 6);
			}
		}
	}
}

void RubikCube::Rotate(int layerId,float angle)
{
	int type=layerId/3;
	D3DXMATRIX*		(WINAPI  *RotationFunc)( D3DXMATRIX *, FLOAT  );
	switch (type)
	{
	case  0:
		RotationFunc=D3DXMatrixRotationX;
		break;
	case 1:
		RotationFunc=D3DXMatrixRotationY;
		break;
	case  2:
		RotationFunc=D3DXMatrixRotationZ;
		break;
	default:
		break;
	}

	for(int i=0;i<27;i++)
	{
		if(m_pCubes[i].OnLayer(layerId))
		{
			D3DXMATRIX temp;
			RotationFunc(&temp,angle);
			m_pCubes[i].UpdateWorldMatrix(temp);

		}
	}

}

void RubikCube::Rotate(int layerId,D3DXVECTOR3& axis,float angle)
{
	for(int i=0;i<27;i++)
		if(m_pCubes[i].OnLayer(layerId))
		{
			D3DXMATRIX temp;
			D3DXMatrixRotationAxis(&temp,&axis,angle);
			m_pCubes[i].UpdateWorldMatrix(temp);
		}
}

void RubikCube::Render()
{
	for(int i=0;i<27;i++)
		m_pCubes[i].Draw();

}


//旋转某一层相关放到最后写：

void RubikCube::OnLeftButtonDown(int x, int y)
{
	if(!m_rotateFinish)
		return;
	m_rotateFinish = false ; // Prevent the other rotation during this rotate

	// Clear total angle
	m_TotalAngle = 0;

	m_PreviousPoint=m_pArcBall->ScreenToBall(x,y);
	m_PreviousVec=ScreenToWorld(m_pDevice,x,y);

	Ray pickingRay=CaculatePickingRay(m_pDevice,x,y);
	D3DXVECTOR3 currentHitPoint;	// hit point on the face
	float minDist = 100000.0f ;
	for(int i=0;i<6;i++)
	{
		if( RayRectIntersection(pickingRay,m_pFaces[i],currentHitPoint) )
		{
			m_isHit=true;

			float distance=SquareDistance(pickingRay.origin,currentHitPoint);
			if(distance<minDist)
			{
				minDist=distance;
				m_StartHitPoint=currentHitPoint;

				//m_PickedFace=i;不行，会报错
				switch (i)
				{
				case 0:
					m_PickedFace=kFrontFace;
					break;
				case 1:
					m_PickedFace=kBackFace;
					break;
				case 2:
					m_PickedFace=kLeftFace;
					break;
				case 3:
					m_PickedFace=kRightFace;
					break;
				case 4:
					m_PickedFace=kTopFace;
					break;
				case 5:
					m_PickedFace=kBottomFace;
					break;
				default:
					m_PickedFace=kUnknownFace;
					break;
				}
			}
		}
	}

	//m_PickedFace=GetPickedFace(m_StartHitPoint);

}

void RubikCube::OnMouseMove(int x, int y)
{
	if(!m_isHit)
		return;

	m_CurrentPoint=m_pArcBall->ScreenToBall(x,y);
	m_IncreamentQuaternion=m_pArcBall->QuatFromBallPoints(m_PreviousPoint,m_CurrentPoint);
	
	m_CurrentVec=ScreenToWorld(m_pDevice,x,y);

	if(!m_cubesSelected)
	{
		m_cubesSelected=true;

		Ray pickingRay=CaculatePickingRay(m_pDevice,x,y);
		RayRectIntersection(pickingRay,m_pFaces[m_PickedFace],m_CurrentHitPoint);

		m_RotateAxis=GetRotateAxis(m_PickedFace,m_StartHitPoint,m_CurrentVec);
		m_HitLayer=GetHitLayer(m_PickedFace,m_RotateAxis,m_StartHitPoint);
	}

	float angle=CalculateRotateAngle();
	m_RotateDirection=GetRotateDirection(m_PickedFace,m_RotateAxis,m_PreviousVec,m_CurrentVec);
	// Flip the angle if the direction is counter-clockwise
	// the positive direction is clockwise around the rotate axis when look through the axis toward the origin.
	if (m_RotateDirection == kCounterClockWise)
		angle = -angle;
	m_TotalAngle+= angle;

	Rotate(m_HitLayer,m_RotateAxis,angle);

	m_PreviousPoint=m_CurrentPoint;
	m_PreviousVec=m_CurrentVec;

}

void RubikCube::OnLeftButtonUp()
{
	m_isHit=false;

	float left_angle = 0.0f ;	// the angle need to rotate when mouse is up
	int   num_half_PI = 0;

	if (m_TotalAngle > 0)
	{
		while (m_TotalAngle >= D3DX_PI / 2)
		{
			m_TotalAngle -= D3DX_PI / 2;
			++num_half_PI;
		}

		if ((m_TotalAngle >= 0) && (m_TotalAngle <= D3DX_PI / 4))
		{
			left_angle = -m_TotalAngle;
		}

		else // ((m_TotalAngle > D3DX_PI / 4) && (m_TotalAngle < D3DX_PI / 2))
		{
			++num_half_PI;
			left_angle = D3DX_PI / 2 - m_TotalAngle;
		}

	}
	else // m_TotalAngle < 0
	{
		while (m_TotalAngle <= -D3DX_PI / 2)
		{
			m_TotalAngle += D3DX_PI / 2;
			--num_half_PI;
		}

		if ((m_TotalAngle >= -D3DX_PI / 4) && (m_TotalAngle <= 0))
		{
			left_angle = -m_TotalAngle;
		}

		else // ((m_TotalAngle > -D3DX_PI / 2) && (m_TotalAngle < -D3DX_PI / 4))
		{
			--num_half_PI;
			left_angle = -D3DX_PI / 2 - m_TotalAngle;
		}
	}

	Rotate(m_HitLayer,m_RotateAxis,left_angle);

	// Make num_rotate_half_PI > 0, since we will mode 4 later
	// so add it 4 each time, -1 = 3, -2 = 2, -3 = 1
	// because - (pi / 2) = 3 * pi /2, -pi / 2 = pi / 2, - 3 * pi / 2 = pi / 2
	while (num_half_PI < 0)
		num_half_PI += 4;

	num_half_PI %= 4;

	for (int i = 0; i <27; ++i)
	{
		if (m_pCubes[i].OnLayer(m_HitLayer))
			m_pCubes[i].UpdatePosition(m_RotateAxis,num_half_PI);
	}

	UpdateLayerId();

	// When mouse up, one rotation was finished, no cube was selected
	m_cubesSelected= false;

	// Enable next rotation.
	m_rotateFinish= true ;
}

void RubikCube::HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_LBUTTONDOWN:
		{	
			// Set current window to capture mouse event, so even if the mouse was release outside the window
			// the message still can be correctly processed.
			SetCapture(hWnd) ;
			int iMouseX = ( short )LOWORD( lParam );
			int iMouseY = ( short )HIWORD( lParam );
			OnLeftButtonDown(iMouseX, iMouseY);
		}
		break ;

	case WM_LBUTTONUP:
		{
			OnLeftButtonUp();
			ReleaseCapture();
		}
		break ;

	case WM_MOUSEMOVE:
		{
			int iMouseX = ( short )LOWORD( lParam );
			int iMouseY = ( short )HIWORD( lParam );
			OnMouseMove(iMouseX, iMouseY) ;
		}
		break ;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'R':
			Restore();
			break;
		case 'S':
			Shuffle();
			break;
		}
		break;

	}
}

Face RubikCube::GetPickedFace(D3DXVECTOR3 hit_point)
{
	float float_epsilon = 0.001f;
	float cube_length = m_pCubes[0].GetLength();

	if (fabs(hit_point.z + m_HalfLength) < float_epsilon) { return kFrontFace;  }
	if (fabs(hit_point.z - m_HalfLength) < float_epsilon) { return kBackFace;   }
	if (fabs(hit_point.x + m_HalfLength) < float_epsilon) { return kLeftFace;   }
	if (fabs(hit_point.x - m_HalfLength) < float_epsilon) { return kRightFace;  }
	if (fabs(hit_point.y - m_HalfLength) < float_epsilon) { return kTopFace;    }
	if (fabs(hit_point.y + m_HalfLength) < float_epsilon) { return kBottomFace; }

	return kUnknownFace;
}

D3DXVECTOR3 RubikCube::GetRotateAxis(Face face, D3DXVECTOR3& previous_point, D3DXVECTOR3& current_point)
{
	float abs_diff_x = fabs(previous_point.x - current_point.x);
	float abs_diff_y = fabs(previous_point.y - current_point.y);
	float abs_diff_z = fabs(previous_point.z - current_point.z);

	switch (face)
	{
	case kFrontFace:
	case kBackFace:
		if (abs_diff_x < abs_diff_y)
			return D3DXVECTOR3(1, 0, 0);
		else
			return D3DXVECTOR3(0, 1, 0);
		break;

	case kLeftFace:
	case kRightFace:
		if (abs_diff_y < abs_diff_z)
			return D3DXVECTOR3(0, 1, 0);
		else
			return D3DXVECTOR3(0, 0, 1);
		break;

	case kTopFace:
	case kBottomFace:
		if (abs_diff_x < abs_diff_z)
			return D3DXVECTOR3(1, 0, 0);
		else
			return D3DXVECTOR3(0, 0, 1);
		break;

	default:
		return D3DXVECTOR3(0, 0, 0);
	}
}

int  RubikCube::GetHitLayer(Face face, D3DXVECTOR3& rotate_axis, D3DXVECTOR3& hit_point)
{
	float length = m_pCubes[0].GetLength();
	float float_epsilon = 0.0001f; 

	// X-Axis 
	if (rotate_axis.x != 0)
	{
		switch (face)
		{
		case kFrontFace:
		case kBackFace:
		case kTopFace:
		case kBottomFace:
			{
				for (int i = 0; i < 3; ++i)
				{
					if (hit_point.x + m_HalfLength >= i * (length + m_CubeGap) 
						&& hit_point.x + m_HalfLength <= (i + 1) * (length + m_CubeGap) - m_CubeGap)
						return i;
				}
			}
			break;
		}
	}

	// Y-Axis
	else if (rotate_axis.y != 0)
	{
		switch (face)
		{
		case kLeftFace:
		case kRightFace:
		case kFrontFace:
		case kBackFace:
			{
				for (int i = 0; i < 3; ++i)
				{
					if (hit_point.y + m_HalfLength >= i * (length + m_CubeGap)
						&& hit_point.y + m_HalfLength <= (i + 1) * (length + m_CubeGap) - m_CubeGap)
						return i + 3;
				}
			}
			break;
		}
	}

	// Z-Axis
	else 
	{
		switch (face)
		{
		case kLeftFace:
		case kRightFace:
		case kTopFace:
		case kBottomFace:
			{
				for (int i = 0; i < 3; ++i)
				{
					if (hit_point.z + m_HalfLength >= i * (length + m_CubeGap)
						&& hit_point.z + m_HalfLength <= (i + 1) * (length + m_CubeGap) - m_CubeGap)
						return i + 6;
				}
			}
			break;
		}
	}

	return -1;
}

float RubikCube::CalculateRotateAngle()
{
	return 2.0f * acosf(m_IncreamentQuaternion.w) * m_RotateSpeed ;
}

RotateDirection RubikCube::GetRotateDirection(Face face, D3DXVECTOR3& axis, D3DXVECTOR3& previous_vector, D3DXVECTOR3& current_vector)
{
	float delta_x = previous_vector.x - current_vector.x;
	float delta_y = previous_vector.y - current_vector.y;
	float delta_z = previous_vector.z - current_vector.z;
	//一开始为了省事用的也是ArcBall上的点来判断，但有时转下去会卡住或者会反向
	//问题出在z的渐变，在y=0或x=0处会发生转折！并不是一直增大或者一直减小

	// Rotate around x-axis
	if (axis.x != 0)
	{
		switch (face)
		{
		case kFrontFace:
			if (delta_y > 0) { return kCounterClockWise; }
			else            { return kClockWise; }
			break;

		case kBackFace:
			if (delta_y < 0) { return kCounterClockWise; }
			else           { return kClockWise; }
			break;

		case kTopFace:
			if (delta_z > 0) { return kCounterClockWise; }
			else            { return kClockWise; }
			break;

		case kBottomFace:
			if (delta_z < 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;
		default:
			return kUnknownDirection;
		}
	}

	// Rotate around y-axis
	else if (axis.y != 0)
	{
		switch (face)
		{
		case kFrontFace:
			if (delta_x < 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		case kBackFace:
			if (delta_x > 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		case kLeftFace:
			if (delta_z > 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		case kRightFace:
			if (delta_z < 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		default:
			return kUnknownDirection;
		}
	}

	// Rotate around z-axis
	else // axis.z != 0
	{
		switch (face)
		{
		case kLeftFace:
			if (delta_y < 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		case kRightFace:
			if (delta_y > 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		case kTopFace:
			if (delta_x < 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		case kBottomFace:
			if (delta_x > 0) { return kCounterClockWise; }
			else             { return kClockWise; }
			break;

		default:
			return kUnknownDirection;
		}
	}
}


void RubikCube::Shuffle()
{

}

void RubikCube::Restore()
{

}





