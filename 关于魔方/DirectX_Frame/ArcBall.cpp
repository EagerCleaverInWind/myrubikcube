#include "ArcBall.h"

ArcBall::ArcBall(float windowWidth,float windowHeight):
	_radius(1.0f),
	_windowWidth(windowWidth),_windowHeight(windowHeight)
{

}

ArcBall::~ArcBall()
{

}

D3DXVECTOR3 ArcBall::ScreenToBall(int screenX,int screenY)
{
	// Scale to screen
	float x = (screenX - _windowWidth/ 2) *_radius/ (_windowWidth / 2);
	float y = -(screenY - _windowHeight / 2)*_radius/ (_windowHeight / 2);

	float z = 0.0f;
	float mag = x * x + y * y;
	float r2=_radius*_radius;
	if(mag >r2 )
	{
		float scale = _radius / sqrtf(mag);
		x *= scale;
		y *= scale;
	}
	else
		z = -sqrtf(r2 - mag);

	return D3DXVECTOR3(x, y, z);
}

D3DXQUATERNION  ArcBall::QuatFromBallPoints(D3DXVECTOR3 &startPoint,D3DXVECTOR3 &endPoint)
{
	// Calculate cos(rotate angle)
	float cos = D3DXVec3Dot(&startPoint, &endPoint);	

	// Calculate rotate axis
	D3DXVECTOR3 axis;
	D3DXVec3Cross(&axis, &startPoint, &endPoint);		

	// Build and Normalize the Quaternion
	D3DXQUATERNION quat(axis.x, axis.y, axis.z, cos);
	D3DXQuaternionNormalize(&quat, &quat);

	return quat;
}
