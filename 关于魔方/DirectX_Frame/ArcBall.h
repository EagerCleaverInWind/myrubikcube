#pragma  once

#include "Utility.h"

#define  MAX_DISTANCE  50
#define  MIN_DISTANCE  200

class ArcBall
{
private:
	float _radius;
	float _windowWidth;
	float _windowHeight;


public:
	ArcBall(float windowWidth,float windowHeight);
	~ArcBall();

	D3DXVECTOR3		ScreenToBall(int screenX,int screenY);
	D3DXQUATERNION		QuatFromBallPoints(D3DXVECTOR3 &previousPoint,D3DXVECTOR3 &currentPoint);

};