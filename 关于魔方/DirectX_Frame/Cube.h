#pragma  once

#include "Utility.h"

#define  CUBE_LENGHTH		10


struct VertexForCube
{
	float x,	y,z;//position
	float nx,ny,nz;//normal
	float u,v;//texture
};

#define  D3DFVF_VertexForCube		(D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)

class Cube
{
private:
	LPDIRECT3DDEVICE9	_pdevice;
	LPDIRECT3DVERTEXBUFFER9  _pVerBuf;
	LPDIRECT3DINDEXBUFFER9	_ppIndexBufs[6];
	static LPDIRECT3DTEXTURE9	_ppFaceTextures[6];
	static LPDIRECT3DTEXTURE9  _pInnerTex;
	int _pTextureId[6];

	float		_length;
	D3DXVECTOR3		_minPoint;
	D3DXVECTOR3		_maxPoint;
	D3DXVECTOR3		_center;

	int _layerX;
	int _layerY;
	int _layerZ;

	D3DXMATRIX	_worldMatrix;


public:
	Cube();//如果将参数LPDIRECT3DDEVICE9 device放在这里，new该类的数组时会因为无默认构造函数而失败
	~Cube();
	
	void Init(LPDIRECT3DDEVICE9 device,D3DXVECTOR3 & front_bottom_left);

	LPDIRECT3DTEXTURE9 CreateTexture(int texWidth, int texHeight,D3DCOLOR color,int sideWidth=10,D3DCOLOR sideColor=0xff000000);
	void ReleaseStaticResources();
	void SetFaceTextures(LPDIRECT3DTEXTURE9 *FaceTextures);
	void SetInnerTexture(LPDIRECT3DTEXTURE9 innerTexture) {_pInnerTex=innerTexture;}
	void SetTextureId(int faceId, int textureId) { _pTextureId[faceId]=textureId; }


	void UpdatePosition(D3DXVECTOR3& rotate_axis,int num_half_PI);//当旋转角是D3DX_PI/2倍数时才更新

	void SetLayerX(int layerId) {_layerX=layerId;}
	void SetLayerY(int layerId) {_layerY=layerId;}
	void SetLayerZ(int layerId) {_layerZ=layerId;}
	bool OnLayer(int layerId);
	void UpdateWorldMatrix(D3DXMATRIX  &append) {_worldMatrix=_worldMatrix*append;}
	void Draw();

	float GetLength() { return _length; }
	D3DXVECTOR3 GetCenter() {return _center;}
};

