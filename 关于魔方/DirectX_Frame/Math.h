#pragma  once

#include "Utility.h"

const float float_epsilon = 0.00001f;

// Triangle
struct Triangle
{
	D3DXVECTOR3 v1 ;
	D3DXVECTOR3 v2 ;
	D3DXVECTOR3 v3 ;

	Triangle(D3DXVECTOR3 v1, D3DXVECTOR3 v2, D3DXVECTOR3 v3)
	{
		this->v1 = v1 ;
		this->v2 = v2 ;
		this->v3 = v3 ;
	}
} ;

// Rectangle
struct Rect 
{
	D3DXVECTOR3 v1 ; // top-left 
	D3DXVECTOR3 v2 ; // top-right
	D3DXVECTOR3 v3 ; // left-bottom
	D3DXVECTOR3 v4 ; // right-bottom

	Rect(){};

	Rect(D3DXVECTOR3 v1, D3DXVECTOR3 v2, D3DXVECTOR3 v3, D3DXVECTOR3 v4)
	{
		this->v1 = v1 ;
		this->v2 = v2 ;
		this->v3 = v3 ;
		this->v4 = v4 ;
	}
};

// Picking ray
struct Ray
{
	D3DXVECTOR3 origin;
	D3DXVECTOR3 direction;

	Ray(){}

	Ray(D3DXVECTOR3 origin, D3DXVECTOR3 direction)
	{
		this->origin    = origin;
		this->direction = direction;
	}
};

// Calculate the square distance of two points
inline float SquareDistance(D3DXVECTOR3 v1, D3DXVECTOR3 v2)
{
	return (v1.x - v2.x) * (v1.x - v2.x) +
		(v1.y - v2.y) * (v1.y - v2.y) +
		(v1.z - v2.z) * (v1.z - v2.z);
}

Ray CaculatePickingRay(LPDIRECT3DDEVICE9 d3ddevice_,int x,int y);

D3DXVECTOR3 ScreenToWorld(LPDIRECT3DDEVICE9 d3ddevice,int x,int y);

bool RayTriangleIntersection(Ray* ray, Triangle* triangle, D3DXVECTOR3* hit_point);

bool RayRectIntersection(Ray& ray, Rect& rect, D3DXVECTOR3& hit_point);
