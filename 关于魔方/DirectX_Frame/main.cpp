#include <tchar.h>
#include "Utility.h"
#include "RubikCube.h"
#include "ArcBallCamera.h"

#pragma comment(lib,"winmm.lib")//调用PlaySound函数所需库文件

#define  WINDOW_WIDTH 800
#define  WINDOW_HEIGHT 600
#define  WINDOW_TITTLE L"RubikCube"
#define  WNDCLASS_NAME L"haha"

//-----------------------------------【全局变量の声明】---------------------------------------
LPDIRECT3DDEVICE9  g_pd3dDevice=NULL;

wchar_t g_strFps[50];
ID3DXFont  *g_pFont=NULL;

RubikCube *g_pRubikCube=NULL;

ABCamera g_ABCamera(WINDOW_WIDTH,WINDOW_HEIGHT,100);

;//------------------------------------------------------------------------------------------------    


//-----------------------------------【函数声明】---------------------------------------
LRESULT  CALLBACK  WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);
HRESULT  Direct3D_Init(HWND hwnd);
LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd);
void  Direct3D_Render(HWND hwnd);
void HelpText_Render(HWND hwnd);
void Direct3D_Update();
void  Direct3D_CleanUp();
float Get_FPS();
//------------------------------------------------------------------------------------------------    



int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nShowCmd)
{
	WNDCLASSEX wndClass={0};
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW|CS_VREDRAW;
	wndClass.lpfnWndProc=WndProc;
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;
	wndClass.hInstance=hInstance;
	wndClass.hIcon=(HICON)::LoadImage(NULL,L"icon.ico",IMAGE_ICON,0,0,LR_DEFAULTSIZE|LR_LOADFROMFILE);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndClass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndClass.lpszMenuName=NULL;
	wndClass.lpszClassName=WNDCLASS_NAME;

	if(!RegisterClassEx(&wndClass))
		return -1;

	HWND hwnd=CreateWindow(WNDCLASS_NAME,WINDOW_TITTLE,WS_OVERLAPPEDWINDOW,0,0,
		WINDOW_WIDTH,WINDOW_HEIGHT,NULL,NULL,hInstance,NULL);
	MoveWindow(hwnd,250,80,WINDOW_WIDTH,WINDOW_HEIGHT,true);
	ShowWindow(hwnd,nShowCmd);
	UpdateWindow(hwnd);

	//PlaySound(L"音频文件名.wav",NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);

	Direct3D_Init(hwnd);
	Object_Init(hInstance,hwnd);


	MSG msg={0};
	while (msg.message!=WM_QUIT)
	{
		if (PeekMessage(&msg,0,0,0,PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Direct3D_Update();
			Direct3D_Render(hwnd);
		}
	}

	UnregisterClass(WNDCLASS_NAME,wndClass.hInstance);
	return 0;
}


LRESULT  CALLBACK  WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	g_ABCamera.HandleMessages(hwnd,message,wParam,lParam,g_pd3dDevice);
	g_pRubikCube->HandleMessages(hwnd,message,wParam,lParam);
	switch (message)
	{
	case   WM_KEYDOWN:
		if(wParam==VK_ESCAPE)
			DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		Direct3D_CleanUp();
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hwnd,message,wParam,lParam);
	}

	return 0;
}

HRESULT  Direct3D_Init(HWND hwnd)
{
	//创建D3D接口
	LPDIRECT3D9		PD3D=NULL;
	if(NULL==(PD3D=Direct3DCreate9(D3D_SDK_VERSION)))
		return E_FAIL;

	//获取硬件设备信息
	D3DCAPS9 caps;
	int vp=0;
	if(FAILED(PD3D->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps) ) )
		return E_FAIL;
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		vp=D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		vp=D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	//填充D3DPRESENT_PARAMETERS结构体
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp,sizeof(d3dpp));
	d3dpp.BackBufferWidth=WINDOW_WIDTH;
	d3dpp.BackBufferHeight=WINDOW_HEIGHT;
	d3dpp.BackBufferFormat=D3DFMT_A8R8G8B8;
	d3dpp.BackBufferCount=1;
	d3dpp.MultiSampleType=D3DMULTISAMPLE_NONE;
	d3dpp.MultiSampleQuality=0;
	d3dpp.SwapEffect=D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow=hwnd;
	d3dpp.Windowed=true;
	d3dpp.EnableAutoDepthStencil=true;
	d3dpp.AutoDepthStencilFormat=D3DFMT_D24S8;
	d3dpp.Flags=0;
	d3dpp.FullScreen_RefreshRateInHz=0;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_IMMEDIATE;

	//创建D3D设备
	if(FAILED(PD3D->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,vp,&d3dpp,&g_pd3dDevice) ) )
		return E_FAIL;

	SAFE_RELEASE(PD3D);
	return S_OK;
}


LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd)
{
	D3DXCreateFont(g_pd3dDevice, 36, 0, 0, 1, false, DEFAULT_CHARSET, 
			   OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, 0, _T("微软雅黑"), &g_pFont);

	g_pRubikCube=new RubikCube(g_pd3dDevice,WINDOW_WIDTH,WINDOW_HEIGHT);

	D3DMATERIAL9 mtrl;
	::ZeroMemory(&mtrl, sizeof(mtrl));
	mtrl.Ambient  = D3DXCOLOR(1.0f,1.0f, 1.0f, 1.0f);
	mtrl.Diffuse  = D3DXCOLOR(1.0f, 1.0f,1.0f, 1.0f);
	mtrl.Specular = D3DXCOLOR(1.0f,1.0f, 1.0f, 1.0f);
	g_pd3dDevice->SetMaterial(&mtrl);

	D3DLIGHT9	 light;  
	::ZeroMemory(&light, sizeof(light));  
	light.Type          = D3DLIGHT_DIRECTIONAL;  
	light.Ambient       = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);  
	light.Diffuse       = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);  
	light.Specular      = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);  
	light.Direction     = D3DXVECTOR3(1.0f, 0.0f, 0.0f);  
	g_pd3dDevice->SetLight(0, &light); 
	g_pd3dDevice->LightEnable(0, true);

	g_pd3dDevice->SetRenderState(D3DRS_LIGHTING,true);
	g_pd3dDevice->SetRenderState(D3DRS_NORMALIZENORMALS, true);   //初始化顶点法线
	g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);   //开启背面消隐
	g_pd3dDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(36, 36, 36));   //设置一下额外环境光 

	//设置纹理过滤
	g_pd3dDevice->SetSamplerState(0,D3DSAMP_MAGFILTER,D3DTEXF_LINEAR);
	g_pd3dDevice->SetSamplerState(0,D3DSAMP_MINFILTER,D3DTEXF_LINEAR);

	D3DXMATRIX matView;
	D3DXVECTOR3 vEye(0.0f, 0.0f, -100.0f);
	D3DXVECTOR3 vAt(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 vUp(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&matView, &vEye, &vAt, &vUp);
	g_pd3dDevice->SetTransform(D3DTS_VIEW, &matView); 

	D3DXMATRIX matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI / 4.0f,(float)((double)WINDOW_WIDTH/WINDOW_HEIGHT),1.0f, 1000.0f);
	g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &matProj);
	return S_OK;
}

void Direct3D_Update()
{

}

void  Direct3D_Render( HWND hwnd)
{
	g_pd3dDevice->Clear(0,NULL,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,D3DCOLOR_XRGB(92,172,238),1.0f,0);
	g_pd3dDevice->BeginScene();

	HelpText_Render(hwnd);

	g_pRubikCube->Render();

	g_pd3dDevice->EndScene();
	g_pd3dDevice->Present(NULL,NULL,NULL,NULL);
}

void HelpText_Render(HWND hwnd)
{
	//FPS显示
	RECT  formatRect;
	GetClientRect(hwnd,&formatRect);
	int charCount=swprintf_s(g_strFps, 20,_T("FPS:%0.3f"),Get_FPS());
	g_pFont->DrawTextW(NULL,g_strFps,charCount,&formatRect,DT_TOP | DT_RIGHT, D3DCOLOR_XRGB(255,39,136));
}


void  Direct3D_CleanUp()
{
	SAFE_DELETE(g_pRubikCube);
	SAFE_RELEASE(g_pFont);
	SAFE_RELEASE(g_pd3dDevice);	
}

float Get_FPS()
{
	static float fps=0;
	static int frameCount=0;
	static float currentTime=0.0,lastTime=0.0;
	++frameCount;
	currentTime=timeGetTime()*0.001f;
	if(currentTime-lastTime>1.0)
	{
		fps=frameCount/(currentTime-lastTime);
		lastTime=currentTime;
		frameCount=0;
	}

	return fps;
}

