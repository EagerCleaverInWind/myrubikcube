#include "Cube.h"

LPDIRECT3DTEXTURE9 Cube::_ppFaceTextures[6];
LPDIRECT3DTEXTURE9 Cube::_pInnerTex;

Cube::Cube()
{
	for(int i=0;i<6;i++)
	{	
		_pTextureId[i]=-1;//default innerTexture
		_ppFaceTextures[i]=NULL;
		_ppIndexBufs[i]=NULL;
	}
	_pdevice=NULL;
	_pVerBuf=NULL;
	_pInnerTex=NULL;

	_length=CUBE_LENGHTH;
	D3DXMatrixIdentity(&_worldMatrix);

}

Cube::~Cube()
{
	for(int i=0;i<6;i++)
		SAFE_RELEASE(_ppIndexBufs[i]);
	SAFE_RELEASE(_pVerBuf);
}

void Cube::Init(LPDIRECT3DDEVICE9 device,D3DXVECTOR3 & front_bottom_left)
{
	//pd3dDevice
	if(!_pdevice)
		_pdevice=device;

	//position
	_minPoint=front_bottom_left;
	_maxPoint=D3DXVECTOR3(_minPoint.x+_length,_minPoint.y+_length,_minPoint.z+_length);
	_center=(_minPoint+_maxPoint)/2.0f;

	//vertexBuffer
	if(!_pdevice)
		return;
	float x = front_bottom_left.x;
	float y = front_bottom_left.y;
	float z = front_bottom_left.z;
	VertexForCube vertices[] =//这里没必要按D3DPT_TRIANGLESTRIP顺序，反正用索引缓存画
	{
		// Front face
		{          x,           y,           z,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f}, // 0
		{          x, y + _length,           z,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f}, // 1
		{x + _length, y + _length,           z,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f}, // 2
		{x + _length,           y,           z,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f}, // 3

		// Back face
		{x + _length,           y, z + _length,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f}, // 4
		{x + _length, y + _length, z + _length,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f}, // 5
		{          x, y + _length, z + _length,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f}, // 6
		{          x,           y, z + _length,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f}, // 7

		// Left face
		{          x,           y, z + _length, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f}, // 8
		{          x, y + _length, z + _length, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f}, // 9
		{          x, y + _length,           z, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f}, // 10
		{          x,           y,           z, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f}, // 11

		// Right face 
		{x + _length,           y,           z,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f}, // 12
		{x + _length, y + _length,           z,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f}, // 13
		{x + _length, y + _length, z + _length,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f}, // 14
		{x + _length,           y, z + _length,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f}, // 15

		// Top face
		{          x, y + _length,           z,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f}, // 16
		{          x, y + _length, z + _length,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f}, // 17
		{x + _length, y + _length, z + _length,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f}, // 18
		{x + _length, y + _length,           z,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f}, // 19

		// Bottom face
		{x + _length,           y,           z,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f}, // 20
		{x + _length,           y, z + _length,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f}, // 21
		{          x,           y, z + _length,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f}, // 22
		{          x,           y,           z,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f}, // 23
	};
	_pdevice->CreateVertexBuffer(sizeof(vertices),D3DUSAGE_WRITEONLY,
		D3DFVF_VertexForCube,D3DPOOL_MANAGED,&_pVerBuf,NULL);
	void *pVertices;
	_pVerBuf->Lock(0,sizeof(vertices),(void**)&pVertices,0);
	memcpy(pVertices,vertices,sizeof(vertices));
	_pVerBuf->Unlock();

	//IndexBuffer
	WORD indicesFront[]  = { 0,  1,  3,  2};
	WORD indicesBack[]   = { 4,  5,  7,  6};
	WORD indicesLeft[]   = { 8,  9, 11, 10};
	WORD indicesRight[]  = {12, 13, 15, 14};
	WORD indicesTop[]    = {16, 17, 19, 18};
	WORD indicesBottom[] = {20, 21, 23, 22};

	WORD* indices[6] = {indicesFront, indicesBack, indicesLeft, indicesRight, indicesTop, indicesBottom};
	for(int i=0;i<6;i++)
	{
		_pdevice->CreateIndexBuffer(sizeof(indicesFront),D3DUSAGE_WRITEONLY,D3DFMT_INDEX16,
			D3DPOOL_DEFAULT,&_ppIndexBufs[i],NULL);
		void *pIndices;
		_ppIndexBufs[i]->Lock(0,sizeof(indicesFront),(void**)&pIndices,0);
		memcpy(pIndices,indices[i],sizeof(indicesFront));
		_ppIndexBufs[i]->Unlock();
	}


}

LPDIRECT3DTEXTURE9 Cube::CreateTexture(int texWidth, int texHeight,D3DCOLOR color,int sideWidth,D3DCOLOR sideColor)
{
	LPDIRECT3DTEXTURE9 pTexture;

	HRESULT hr = D3DXCreateTexture(_pdevice, 
		texWidth, 
		texHeight, 
		0, 
		0, 
		D3DFMT_A8R8G8B8,  // 4 bytes for a pixel 
		D3DPOOL_MANAGED, 
		&pTexture);

	if (FAILED(hr))
	{
		MessageBox(NULL, L"Create texture failed", L"Error", 0);
	}

	// Lock the texture and fill in color
	D3DLOCKED_RECT lockedRect;
	hr = pTexture->LockRect(0, &lockedRect, NULL, 0);
	if (FAILED(hr))
	{
		MessageBox(NULL, L"Lock texture failed!", L"Error", 0);
	}


	// Calculate number of rows in the locked Rect
	int rowCount = (texWidth * texHeight * 4 ) / lockedRect.Pitch;

	for (int i = 0; i < texWidth; ++i)
	{
		for (int j = 0; j < texHeight; ++j)
		{
			int index = i * rowCount + j;

			int* pData = (int*)lockedRect.pBits;

			if (i <= sideWidth || i >= texWidth -  sideWidth
				|| j <=  sideWidth || j >= texHeight -  sideWidth)
			{
				memcpy(&pData[index], &sideColor, 4);
			}
			else
			{
				memcpy(&pData[index], &color, 4);
			}
		}
	}

	pTexture->UnlockRect(0);

	return pTexture;
}

void Cube::SetFaceTextures(LPDIRECT3DTEXTURE9 *FaceTextures)
{
	for(int i=0;i<6;i++)
	{
		_ppFaceTextures[i]=FaceTextures[i];
	}
}

void Cube::ReleaseStaticResources()
{
	for(int i=0;i<6;i++)
		SAFE_RELEASE(_ppFaceTextures[i]);
	SAFE_RELEASE(_pInnerTex);
}

void Cube::UpdatePosition(D3DXVECTOR3& rotate_axis,int num_half_PI)
{
	//D3DXMATRIX rotateMatrix;
	//D3DXMatrixIdentity(&rotateMatrix);
	//D3DXMatrixRotationAxis(&rotateMatrix,&rotate_axis,num_half_PI*D3DX_PI/2.0f);
	//D3DXVec3TransformCoord(&_center,&_center,&rotateMatrix);
	////一开始用的是D3DXVec3TransformCoord(&_center,&_center,&_worldMatrix)，不过累积下去误差会越来越大

	//_minPoint=D3DXVECTOR3(_center.x-_length/2.0f,_center.y-_length/2.0f,_center.z-_length/2.0f);
	//_maxPoint=D3DXVECTOR3(_center.x+_length/2.0f,_center.y+_length/2.0f,_center.z+_length/2.0f);
	


	// Build up the rotation matrix with the overall angle
	// This angle is times of D3DX_PI / 2.
	D3DXMATRIX rotate_matrix;
	D3DXMatrixIdentity(&rotate_matrix);

	if (num_half_PI == 0)
	{
		D3DXMatrixRotationAxis(&rotate_matrix, &rotate_axis, 0);
	}
	else if (num_half_PI == 1)
	{
		D3DXMatrixRotationAxis(&rotate_matrix, &rotate_axis, D3DX_PI / 2);
	}
	else if (num_half_PI == 2)
	{
		D3DXMatrixRotationAxis(&rotate_matrix, &rotate_axis, D3DX_PI);
	}
	else // (num_half_PI == 3)
	{
		D3DXMatrixRotationAxis(&rotate_matrix, &rotate_axis, 1.5f * D3DX_PI);
	}

	// Translate the min_point_ and max_point_ of the cube, after rotation, the two points 
	// was changed, need to recalculate them with the rotation matrix.
	D3DXVECTOR3 min_point;
	D3DXVECTOR3 max_point;
	D3DXVec3TransformCoord(&min_point, &_minPoint, &rotate_matrix);
	D3DXVec3TransformCoord(&max_point, &_maxPoint, &rotate_matrix);

	// After translate by the world matrix, the min/max point need recalculate
	_minPoint.x = min(min_point.x, max_point.x);
	_minPoint.y = min(min_point.y, max_point.y);
	_minPoint.z = min(min_point.z, max_point.z);

	_maxPoint.x = max(min_point.x, max_point.x);
	_maxPoint.y = max(min_point.y, max_point.y);
	_maxPoint.z = max(min_point.z, max_point.z);

	_center=(_minPoint+_maxPoint)/2;
}

bool Cube::OnLayer(int layerId)
{
	if(layerId==_layerX||layerId==_layerY||layerId==_layerZ)
		return true;
	else
		return false;
}

void Cube::Draw()
{
	_pdevice->SetTransform(D3DTS_WORLD,&_worldMatrix);

	_pdevice->SetFVF(D3DFVF_VertexForCube);
	_pdevice->SetStreamSource(0,_pVerBuf,0,sizeof(VertexForCube));
	for(int i=0;i<6;i++)
	{
		if(_pTextureId[i]<0)
			_pdevice->SetTexture(0,_pInnerTex);
		else
			_pdevice->SetTexture(0,_ppFaceTextures[_pTextureId[i] ]);

		_pdevice->SetIndices(_ppIndexBufs[i]);
		_pdevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP,0,0,24,0,2);
	}
}

