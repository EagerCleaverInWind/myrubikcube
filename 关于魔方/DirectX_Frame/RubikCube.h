#pragma  once
#include "Cube.h"
#include "ArcBall.h"
#include "Math.h"

#define  CUBE_GAP	0.15f

#define  TEX_WIDTH		128
#define  TEX_HEIGHT		128
#define  SIDE_WIDTH		10


// The 6 faces of the Rubik Cube
enum Face
{
	kFrontFace =  0,
	kBackFace  =  1,
	kLeftFace  =  2,
	kRightFace =  3,
	kTopFace   =  4,
	kBottomFace = 5,

	kUnknownFace = 10
};

enum RotateDirection
{
	kClockWise = 0,
	kCounterClockWise = 1,

	kUnknownDirection = 2
};

class RubikCube
{
private:
	LPDIRECT3DDEVICE9 m_pDevice;

	Cube		*m_pCubes;

	float		m_CubeGap;
	float		m_Group;
	float		m_HalfLength;
	int			m_TexWidth; // Texture width in pixel.
	int			m_TexHeight;// Texture height in pixel.
	int			m_SideWidth;//in pixel

	//旋转某一层相关定义
	ArcBall *m_pArcBall;
	float m_WindowWidth;
	float	m_WindowHeight;
	Rect m_pFaces[6];

	bool m_isHit;
	bool m_rotateFinish;
	bool m_cubesSelected;

	
	D3DXVECTOR3 m_PreviousPoint,m_CurrentPoint;
	D3DXQUATERNION m_IncreamentQuaternion;
	float m_RotateSpeed;
	float m_TotalAngle;// The angle rotated when mouse up

	D3DXVECTOR3 m_StartHitPoint,m_CurrentHitPoint;
	Face m_PickedFace;
	D3DXVECTOR3 m_RotateAxis;
	int m_HitLayer;

	D3DXVECTOR3 m_PreviousVec,m_CurrentVec;
	RotateDirection m_RotateDirection;


private:
	void InitCubes();//并填充Cube类的静态成员

	//旋转某一层相关函数
	void UpdateLayerId();
	void Rotate(int layerId,float angle);//update Cube._worldMatrix
	void Rotate(int layerId,D3DXVECTOR3& axis,float angle);

	void OnLeftButtonDown(int x, int y);
	void OnMouseMove(int x, int y);
	void OnLeftButtonUp();


	Face GetPickedFace(D3DXVECTOR3 hit_point);// Get the face picking by mouse 
	D3DXVECTOR3 GetRotateAxis(Face face, D3DXVECTOR3& previous_point, D3DXVECTOR3& current_point);
	int  GetHitLayer(Face face, D3DXVECTOR3& rotate_axis, D3DXVECTOR3& hit_point);
	float CalculateRotateAngle();
	RotateDirection GetRotateDirection(Face face, D3DXVECTOR3& axis, D3DXVECTOR3& previous_vector, D3DXVECTOR3& current_vector);

	void Shuffle();
	void Restore(); 

public:
	RubikCube(LPDIRECT3DDEVICE9 device,float windowWidth,float windowHeight);//为m_pCubes分配内存
	~RubikCube();

	void Render();

	//旋转某一层相关函数
	void HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
};